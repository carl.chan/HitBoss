import pyodbc 


_ConnectionString = r'DRIVER={ODBC Driver 17 for SQL Server};SERVER=127.0.0.1;DATABASE=HitBoss;UID=sa;PWD=581762Ch@n'

def GetDBConnection():
    conn = pyodbc.connect(_ConnectionString,autocommit=True)
    cursor = conn.cursor()
    return cursor

def ExecuteSQL(Statement):
    cursor = GetDBConnection()
    cursor.execute(Statement)
    return cursor

def ExecuteManySQLNonQuery(Statement,Paramters):
    cursor = GetDBConnection()
    cursor.fast_executemany= True
    cursor.executemany(Statement,Paramters)
    return cursor

def ExecuteSQL_Json(Statement):
    cursor = GetDBConnection()
    cursor.execute(Statement)
    Columns = [column[0] for column in cursor.description]
    Result = []
    for row in cursor.fetchall():
        Result.append(dict(zip(Columns, row)))
    cursor.commit()    
    return Result

def ExecuteSQLList(Statement):
    cursor = GetDBConnection()
    cursor.execute(Statement)
    
    rows = cursor.fetchall()
    rowslist = []
    for x in range(0,len(rows)):
        rowslist.append(rows[x])
    return rowslist


def ExecuteStoreProcedure(Statement,Params):
    conn = pyodbc.connect(_ConnectionString,autocommit=True)
    cursor = conn.cursor()
    #params format = (14, "Dinsdale")
    Result = cursor.execute(Statement, Params)
    #conn.commit()
    return Result

