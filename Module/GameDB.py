import Module.DB as DB


def GetGame():
    sql = 'select id,name,description from Game'
    Result = DB.ExecuteSQL(sql)
    return Result

def GetGameZone(g_id):
    sql = "select id,g_id,name,description from GameZone where g_id={}".format(g_id)
    Result = DB.ExecuteSQL(sql)
    return Result

def GetGameZoneTopic(z_id):
    sql = "select id,g_id,z_id,name,description from GameZoneTopic where z_id = {}".format(z_id)
    Result = DB.ExecuteSQL(sql)
    return Result

def GetGameZoneTopicPost(t_id):
    sql = "select id,g_id,z_id,name,description from GameZonePost where t_id = {}".format(t_id)
    Result = DB.ExecuteSQL(sql)
    return Result

def GetItemHeaderByGameID(g_id):
    sql ="SELECT [type],[name], [description] FROM propeties GP inner join Propeties P on GP.prop_id = P.id and GP.g_id = {} and GP.type ='I'".format(g_id)
    Result = DB.ExecuteSQL(sql)
    return Result

def GetCharactersByGameID(g_id):
    sql ="SELECT id, [name],[description],thumb,image,created,created_by FROM [HitBoss].[dbo].[Character] WHERE g_id = {}".format(g_id)
    Result = DB.ExecuteSQL(sql)
    return Result
