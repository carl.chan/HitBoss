from flask import Flask, render_template, request,redirect, url_for
import Module.GameDB as DB

_GameID = 1

def Index():
    Games = DB.GetGame()
    Result = Games.fetchall()
    return render_template('Index.htm',Games = Result)

def Home(id):
    Zones = DB.GetGameZone(id)
    Result = Zones.fetchall()
    return render_template('Game/Home.htm',Zones = Result)

def Zone(z_id):
    Topics = DB.GetGameZoneTopic(z_id)
    Result = Topics.fetchall()
    return render_template('Game/Zone.htm',Topics = Result)

def Topic(t_id):
    Posts = DB.GetGameZoneTopicPost(t_id)
    Result = Posts.fetchall()
    return render_template('Game/Post.htm',Posts = Result)

def Post(p_id):
    Result=[]
    return render_template('Game/Post.htm',Posts = Result)

def Game_Post_Create(t_id):
    ItemHeadersQuery = DB.GetItemHeaderByGameID(_GameID)
    ItemHeaders = ItemHeadersQuery.fetchall()
    CharactersQuery = DB.GetCharactersByGameID(_GameID)
    Characters = CharactersQuery.fetchall()



    return render_template('Game/Post_Create.htm',ItemHeaders = ItemHeaders,Characters=Characters)