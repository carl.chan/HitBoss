from flask import Flask, render_template, request,redirect, url_for
import Module.DB as DB


def GameTeamBossFight():
    Step = []
    Step.append(['1','隨便','隨便','換賽勒斯','隨便','-','-'])
    Step.append(['2','-','上前','換阿佐美','隨便','-','梅菲斯托'])
    Step.append(['3','-','絕命一擊','飛天之舞','-','高級疾風','豬小弟'])
    Step.append(['4','-','絕命一擊','飛天之舞','幸運拳','-','後退'])
    Step.append(['5','絕命一擊','飛天之舞','-','-','高級疾風','瑪娜上前'])
    Step.append(['6','後退','後退','後退','-','-','灰姑娘'])
    Step.append(['7','梅菲斯托','艾米上前','阿佐美上前','摩爾加納上前','-','-'])
    Step.append(['8','梅菲斯托','艾米上前','阿佐美上前','摩爾加納上前','-','-'])
    Step.append(['9','AF','AF','AF','AF','-','-'])

    Item = []
    Item.append(['武器','','','','','',''])
    Item.append(['防具','','','','','',''])
    Item.append(['徽章','','','','','',''])
    Item.append(['技能 1','','','','','',''])
    Item.append(['技能 2','','','','','',''])
    Item.append(['技能 3','','','','','',''])
    Item.append(['靈晶 1','','','','','',''])
    Item.append(['靈晶 2','','','','','',''])
    Item.append(['靈晶 3','','','','','',''])
    Item.append(['靈晶 4','','','','','',''])



    return render_template("Game/BossFight.htm", Steps = Step,Items=Item)