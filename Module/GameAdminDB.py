import Module.DB as DB


def GetGame():
    sql = 'select id,name,description from Game'
    Result = DB.ExecuteSQL(sql)
    return Result

def GetPropeties(g_id):
    sql = "select * from Propeties WHERE  g_id = {} AND name is not null \
    order by seq".format(g_id)
    Result = DB.ExecuteSQL(sql)
    return Result

#Update Propeties by Array
def UpdatePropeties(PropetyData):
    # PropetyData = {}
    # PropetyData['Name'] = PropetyName
    # PropetyData['Description'] = PropetyDescription
    # PropetyData['DataType'] = PropetyDataType
    # PropetyData['ID'] = Propety_ID
    sql = "update Propeties set name = '{}', description = '{}', data_type='{}' where id = {}".format(PropetyData['Name'],PropetyData['Description'],PropetyData['DataType'],PropetyData['ID'])
    Result = DB.ExecuteSQL(sql)
    return Result


def BulkUpdateItems(ItemsData):
    # Accept Dict Array : Sample:
    # InsertItem = []
    # ItemData = {}
    # ItemData['Name'] = ItemName[i]
    # ItemData['Description'] = ItemDescription[i]         
    # ItemData['ID'] = ItemID[i]
    # InsertItem.append(ItemData)
    List = []
    for item in ItemsData:
        List.append([item['name'],item['description'],item['id']])

    if len(List) >0:
        SQL= "update Items set name = ?, description = ? where id = ?"
        DB.ExecuteManySQLNonQuery(SQL,List)
    return True

def BulkInsertItems(ItemsData):
    List = []
    for item in ItemsData:
        List.append([item['g_id'],item['prop_id'],item['name'],item['description']])

    SQL = "INSERT INTO [dbo].[Items] ([g_id] ,[prop_id],[name],[description],[type],[created_by]) VALUES (?,?,?,?,'',0)"
    DB.ExecuteManySQLNonQuery(SQL,List)
    return True    
    

def GetPropetiesByPropID(PropID):
    sql = "select * from Propeties WHERE  id = {} ".format(PropID)
    Result = DB.ExecuteSQL(sql)
    return Result

def GetItemsByPropID(PropID):
    sql = " select * from items where prop_id = {}".format(PropID)
    Result = DB.ExecuteSQL(sql)
    return Result

def GetCharacters(g_id):
    sql = "SELECT * FROM [Character] WHERE g_id = {}".format(g_id)
    Result = DB.ExecuteSQL(sql)
    return Result

def GetCharacterByID(g_id,c_id):
    sql = "SELECT * FROM [Character] WHERE g_id = {} and id ={}".format(g_id,c_id)
    Result = DB.ExecuteSQL(sql)
    return Result



