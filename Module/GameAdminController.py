from flask import Flask, render_template, request,redirect, url_for
import Module.GameDB as DB
import Module.GameAdminDB as GADB

def Index():
    Games = DB.GetGame()
    Result = Games.fetchall()
    return render_template('Index.htm',Games = Result)

def Home(id):
    Zones = DB.GetGameZone(id)
    Result = Zones.fetchall()
    return render_template('Game/Home.htm',Zones = Result)

def PropetiesList(g_id):
    PropetiesDB = GADB.GetPropeties(g_id)
    Propeties = PropetiesDB.fetchall()

    #for navigation
    Navigation = GetNavigation(g_id)

    return render_template('GameAdmin/PropetiesList.htm',Navigation=Navigation,Propeties = Propeties)

def Propeties(g_id,p_id):
    Propety = []
    p_id = int(p_id)
    if( p_id != -1 and p_id >= 1):
        PropetyDB = GADB.GetPropetiesByPropID(p_id)
        Propety = PropetyDB.fetchone()

    ItemsDB = GADB.GetItemsByPropID(p_id)
    Items = ItemsDB.fetchall()

    #for navigation
    Navigation = GetNavigation(g_id)

    return render_template('GameAdmin/Propeties.htm',Navigation = Navigation ,Propety = Propety,Items=Items)

def UpdatePropeties(g_id,prop_id,request):
    PropetyName= request.form.get('Propety_Name')
    PropetyDescription = request.form.get('Propety_Description')
    PropetyDataType = request.form.get('Propety_DataType')
    Propety_ID = request.form.get('Propety_ID')

    PropetyData = {}
    PropetyData['Name'] = PropetyName
    PropetyData['Description'] = PropetyDescription
    PropetyData['DataType'] = PropetyDataType
    PropetyData['ID'] = Propety_ID

    GADB.UpdatePropeties(PropetyData)


    ItemID = request.form.getlist('item_id')
    ItemName = request.form.getlist('item_name')
    ItemDescription =request.form.getlist('item_description')

    UpdateItem = []
    InsertItem = []
    
    for i in range(len(ItemID)):
        ItemData = {}
        ItemData['name'] = ItemName[i]
        ItemData['description'] = ItemDescription[i]         
        ItemData['id'] = ItemID[i]
        ItemData['g_id'] = g_id
        ItemData['prop_id'] = Propety_ID       

        if(ItemID[i]=='0'): #If Item = 0 mean new item           
            InsertItem.append(ItemData)
        else:
            UpdateItem.append(ItemData)

    if(len(UpdateItem)>0):
        GADB.BulkUpdateItems(UpdateItem)
    if(len(InsertItem)>0):
        GADB.BulkInsertItems(InsertItem)

    return '更新成功'


def Character(g_id,c_id):
    Character = []
    c_id = int(c_id)

    # -1 mean new creation, no need to load character information
    if( c_id != -1 and c_id >= 1):
        CharacterDB = GADB.GetCharacterByID(g_id,c_id)
        Character = CharacterDB.fetchone()

    PropetiesDB = GADB.GetPropeties(g_id)
    Propeties = PropetiesDB.fetchall()

    #for navigation
    Navigation = GetNavigation(g_id)

    return render_template('GameAdmin/Character.htm',Navigation = Navigation,Character=Character,Propeties=Propeties)

def CharacterList(g_id,c_id):
    CharactersDB = GADB.GetCharacters(g_id)
    Characters = CharactersDB.fetchall()

    #for navigation
    Navigation = GetNavigation(g_id)


    return render_template('GameAdmin/CharacterList.htm',Navigation=Navigation,Characters = Characters)

def Navigation(g_id):
    Propeties = GADB.GetPropeties(g_id)
    Result = Propeties.fetchall()
    return render_template('GameAdmin/Navigation.htm',Propeties = Result)

def Json_Propeties():
    return ""

def GetNavigation(g_id):
    Propeties = GADB.GetPropeties(g_id)
    Result = Propeties.fetchall()
    return Result
