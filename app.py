from flask import Flask, render_template, request,redirect, url_for
from markupsafe import escape
import Module.GameController as Game
import Module.GameAdminController as GameAdmin


app = Flask(__name__)

# Set the secret key to some random bytes. Keep this really secret!
app.secret_key = b'_2!y3L"F4S8z\n\xec]/'

_gameID = 1

@app.route('/')
def index():
    return Game.Index()

@app.route("/Game/<g_id>")
def Game_Index(g_id):
    return Game.Home(g_id)

@app.route('/Zone/<z_id>')
def Game_Zone(z_id):
    return Game.Zone(z_id)

@app.route('/Topic/<t_id>')
def Game_Topic(t_id):
    return Game.Topic(t_id)

@app.route('/Post')
def GameBossFight():
    return render_template("Game/Post.htm")

@app.route('/Post/Create/<t_id>')
def Game_Post_Create(t_id):
    return Game.Game_Post_Create(t_id)



#Game Admin

@app.route('/Admin')
def AdminHome():
    return render_template("GameAdmin/Home.htm")

@app.route('/Admin/Game/<g_id>')
def AdminGame(g_id):
    return render_template("GameAdmin/Home.htm")

@app.route('/Admin/Game/<g_id>/Propeties/<prop_id>', methods=['POST', 'GET'])
def AdminGamePropeties(g_id,prop_id):
    if request.method == 'POST':
        return GameAdmin.UpdatePropeties(g_id,prop_id,request)
    else:
        if(prop_id.upper()=='LIST'):
            return GameAdmin.PropetiesList(g_id)
        elif(prop_id.upper()=='CREATE'):
            return GameAdmin.Propeties(g_id,0)
        else:
            return GameAdmin.Propeties(g_id,prop_id)

@app.route('/Admin/Game/<g_id>/Character/<c_id>')
def AdminGameCharacter(g_id,c_id):
    if(c_id.upper()=='LIST'):
        return GameAdmin.CharacterList(g_id,c_id)
    elif(c_id.upper()=='CREATE'):
        return GameAdmin.Character(g_id,0)
    else:
        return GameAdmin.Character(g_id,c_id)

##############

@app.route('/Game/Boss/Team')
def GameBossTeam():
    return render_template("Game/Topic.htm")


@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r

if __name__ == '__main__':
    app.run(debug=True)